﻿Yachiyo: Nunca se me hubiera ocurrido@anotarlos en un mapa así...
Yachiyo: Es increíble verlos expuestos de esta forma...
Iroha: Si, es muy interesante como los [textBlue:Rumors]@estan dispersos.
Yachiyo: Cierto, la distribución parece deliberada.
Yachiyo: ―Yachiyo―@¿Me pregunto qué significa todo esto?
Yachiyo: ―Yachiyo―@Se detienen en este punto en particular.
Iroha: ―Iroha―@Me recuerda al ojo de una tormenta.
Yachiyo: Es difícil asegurarlo porque@no sabemos cómo se dispersan en el este.
Yachiyo: Pero si es similar allí también, entonces me pregunto@si tiene algún significado.
Iroha: ¿Hay alguna razón para que se corten así?
Yachiyo: Este lugar es peculiar también, ¿no lo es?
Iroha: ―Iroha―@Si, los rumores se juntan con un poco más de@densidad en Shinsei Ward.
Yachiyo: ―Yachiyo―@Si. A diferencia del este, se siente@como si se juntaran en este punto.
Iroha: ―Iroha―@Este lugar...
Yachiyo: ¿Iroha?
Yachiyo: ¿Conoces este lugar?
Iroha: Yes. Es el Centro Medico Satomi.
Yachiyo: Es donde estaba tu hermana menor.
Iroha: Yachiyo...
Yachiyo: Si, deberíamos ir allá.