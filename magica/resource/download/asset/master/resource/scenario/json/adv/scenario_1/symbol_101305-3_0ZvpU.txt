Iroha: Da un poco de miedo venir aquí de noche.@No se parece en nada al otro día.
Yachiyo: Es pasadas las horas de visita,@así que todas las luces están apagadas.
Yachiyo: Un templo tranquilo rodeado de silencio...@Me parece algo agradable.
Yachiyo: Aunque estando con alguien como ella@arruina la atmósfera un poco.
Tsuruno: ¡Nunca había venido a un templo de noche!
Tsuruno: En la oscuridad, rodeado de árboles... ¡Se@siente como si estuviéramos lejos de la ciudad!
Tsuruno: ¿Crees que vaya a haber@fantasmas o espíritus o algo?
Tsuruno: ¡Tal vez el chico de@la leyenda aparezca!
Iroha: Oye, ¡no digas cosas@espantosas como esas, Tsuruno!
Yachiyo: Vamos Tsuruno, es peligroso@moverse en la oscuridad de esa manera.
Tsuruno: No te preocupes, solo es una colina de grab—@¡Agh!
Tsuruno: ¡Owww!
Yachiyo: ¡Justo cuando lo dije!
Iroha: [chara:100102:effect_emotion_surprise_0][se:7226_shock]¡Tsuruno! ¡¿Estás bien?!
Tsuruno: [chara:100301:effect_emotion_sad_0][se:7224_fall_into]Sí, solo me embarré un poco...
Yachiyo: *Suspiro*@Después lo enjuagamos.
Tsuruno: Ah, ¡la puerta del jardín interior!@¡Realmente está cerrada de noche!
Iroha: (¡Se recupera rápido!)
Yachiyo: Por eso no lo descubrí antes.
Iroha: Ya que lo pienso,@no podemos entrar, ¿o sí?
Yachiyo: Pero tenemos que entrar, ¿cierto?
Iroha: ¿Ehh?@Quiere decir...
Yachiyo: Me alegra que pienses rápido.@Sí, nos vamos a colar.
Iroha: [chara:100102:effect_emotion_surprise_0][se:7226_shock]¡¿Así de verdad lo haremos?!
Iroha: ¡P-pero nos meteremos en problemas!
Yachiyo: Bueno, Tsuruno y yo vamos.@¿Si vienes, verdad Tsuruno?
Tsuruno: ¡Claro!
Yachiyo: ¿Y tú, Iroha?
Yachiyo: ¿Estás bien con no ir?@Pensé que querías ver a tu hermanita.
Yachiyo: Además de que me molesté en invitarte...
Iroha: Urghg... bien, ¡Ya voy!
Yachiyo: Muy bien.
Iroha: (Por favor no dejes que nos descubra un sacerdote...)
Iroha: (Esa oración parece contradictoria, de alguna manera)
