Yachiyo: Me voy a adelantar.@Tómense tu tiempo.
Momoko: Ugg, es rápida...
Momoko: ¡Iroha, démonos prisa!
Iroha: Ah, um... ¡Espera!
Momoko: ¡¿Por qué te detienes?!@No tenemos tiempo para...
Iroha: ...
Momoko: ¿Iroha?
Lil' Kyubey: ¿Mokyu?
Iroha: ¡Está ahí!@¡El pequeño Kyubey!
Momoko: ¡Iroha!@¡La bruja es lo primero!
Iroha: Pero es la razón por la que vine aquí.
Iroha: Todo lo que tengo que hacer es hablar con él...@Entonces podré volver a casa.
Momoko: No es tan simple.
Momoko: Iroha, sé que puede tener@algo que ver con tu deseo...
Momoko: Pero no está garantizado que lo entiendas@todo después de hablar con él.
Iroha: Podrías tener razón... ¡Pero aún así!
Momoko: Si tienes que volver a Kamihama,@Yachiyo sin duda interferirá de nuevo.
Momoko: Así que ocupémonos primero de la bruja.
Iroha: ...
Lil' Kyubey: ¡Mokyu!
Iroha: ¿Kyubey...?
Lil' Kyubey: ¡Kyu, mokkyu!
Momoko: ¿Qué significa eso?
Iroha: ¿Nos estás diciendo que te sigamos?
Lil' Kyubey: ¡Kyu!
Iroha: Espera, ¿nos llevas a la bruja?
Lil' Kyubey: ¡Mokkyu!
Momoko: Hmm, ya veo...
Momoko: Así que el pequeño también quiere que@nos centremos en derrotar a la bruja.
Iroha: ¿Es eso lo que quieres...?
Lil' Kyubey: ¡Kyu!
Iroha: ...Si.
Iroha: Muy bien. ¡Entonces, indícanos el camino!
Momoko: Realmente no estoy segura de que@debamos creerle al pequeño...
Iroha: Pero si Kyubey sabe dónde está la bruja,@no nos perderemos.
Momoko: Es cierto. Luchando de forma normal,@va a ser difícil seguir el ritmo de Yachiyo...
Momoko: Así que tal vez deberíamos arriesgarnos.
Iroha: ¡Está bien!
