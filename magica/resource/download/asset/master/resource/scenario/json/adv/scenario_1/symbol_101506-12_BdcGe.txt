Iroha: Se siente como si un huracán nos hubiera arrasado.
Yachiyo: Sí...
Iroha: No puedo creer que Magius estén protegiendo@Brujas.
Yachiyo: Sí, es despreciable.
Yachiyo: ¿Venciste a esa Bruja, Felicia?
Felicia: ¡Sí, fue pan comido!
Tsuruno: Aunque le tienes terror a Alina.
Felicia: [chara:100501:effect_emotion_surprise_0][se:7226_shock]¡¿Huh?! ¡No le tengo terror! Solo estaba un poquito@sacada de onda...
Yachiyo: Hiciste un buen trabajo.
Iroha: ¿Pero... qué hay de Mami?
Felicia: Ah, la que buscabas antes.@No creo que estuiera ahí dentro.
Homura: ¿Qué hacemos, Madoka?
Homura: No crees que Mami esté... ya sabes, como es chica@Alina insinuó...
Madoka: No.
Madoka: No te preocupes, Homura.
Madoka: La Mami que conozco no caería tan@facilmente contra una Bruja.
Madoka: Solo tenemos que creer en ella. Sé que ella@va a estar bien.
Homura: Sí, tienes razón.
Homura: Mami definitivamente es una Chica Mágica muy@poderosa.
Homura: Así que creo que está bien...
Homura: No, yo sé que así es.
Madoka: ¡Cierto!
Iroha: Yo creo que está bien también.
Iroha: Mami era increiblemente fuerte cuando peleé contra@ella antes.
Iroha: Así que debe estar por ahí en algún lugar.
Madoka: Gracias, Iroha.
Madoka: Nosotras regresaremos a Mitakihara, pero si Mami@todavía no ha regresado...
Madoka: ...creo que regresaremos a Kamihama de nuevo.
Madoka: ¿Nos ayudarían si lo necesitamos entonces?
Iroha: Claro.
Iroha: Madoka, Homura, las dos son mis amigas.
Madoka: Sí... Gracias.
Homura: Muchísimas gracias, Iroha.
Iroha: *Risita*
Homura: Um, Iroha, ¿te parece bien si te pregunto@una cosa?
Iroha: ¿Qué cosa?
Homura: ¿Piensas que las Chicas Mágicas de verdad pueden ser@salvadas en Kamihama?
Iroha: Pues... honestamente no sé a ciencia cierta.
Iroha: Ni siquiera sé a lo que se refieren con salvar a@las Chicas Mágicas.
Iroha: Pero lo que están haciendo las Alas de Magius@no puede ser correcto...
Homura: Ya veo...
Iroha: ¿Estás interesada en lo que tienen que decir?
Homura: Ah, no, no es así. Solo me@lo preguntaba.
Homura: ...
Madoka: Okay, ¿Nos vamos?
Homura: Sí, vámonos.
Homura: Ah, cierto... Madoka, ¿llamaste a@tus padres para decirles que llegarías tarde?
Madoka: [chara:200101:effect_emotion_surprise_0][se:7226_shock]¡¿...?!
Madoka: ¡Oh no! ¡Se me olvidó llamarles!
Homura: ¡Entonces hay que apresurarnos!
Madoka: ¡S-sí!
Madoka: ¡Nos vemos, Iroha!
Homura: Sí, hasta la próxima.
Iroha: ¡Nos vemos chicas!
Homura: ...
