﻿Iroha: (Incluso después de investigar todos los [textBlue:Rumores] que@derrotamos, no tenemos ninguna pista...)
Iroha: (Tampoco le seguimos el rastro a Tsuruno cuando@fuimos a Banbanzai...)
Iroha: (Estos últimos días solo hemos estado pensando@en círculos, sin pistas.)
White Feather: Llamenlas. Busquen alto y bajo...@Pero nunca recuperarás a tus amigas.
Iroha: ...
Tsuruno: Hey Iroha, ¡mira! ¡Hice una bandera para@promover a Banbanzai!
Felicia: ¡Hey! Yo la dibuje,¡¿okay?!@¡¿No se ve INCREÍBLE el cerdo que dibuje?!
Sana: ¡Y si yo la sostengo nadie me verá!
Sana: Será como un truco de magia. ¡Podría ser buena@propaganda también!
Tsuruno: ¡Nos vamos a inundar de clientes!
Felicia: ¡Si! ¡Es lo mejor que hay!
Yachiyo: ¿Por qué no se concentran en la comida primero?
Iroha: *Llanto*
Iroha: ¡No! Mantente firme.
Iroha: (Soy la lider ahora. Tengo que ser más fuerte@que esto.)
Iroha: "Llamenlas. Busquen alto y bajo...@Pero nunca recuperaras a tus amigas.."
Iroha: No lo creo.
Iroha: No creeré en nada que ellas digan, a menos que lo vea yo misma.
Iroha: Si...
Iroha: Realmente necesitamos encontrar a las Alas de Magius y preguntarles directamente.
Iroha: Tenemos que seguir buscando [textBlue:Rumores].
Iroha: [chara:100104:lipSynch_1][chara:100104:voiceFull_fullvoice/section_101704/vo_full_101704-1-22][textBlue:Rumores]...
Ai: >｜IF Y0U M3M3AN AS AN UWASA｜@>｜TH3N Y3s_1 d0 KN0W U1 TAMAK1...｜ <---Adaptar a lo que dice el 1016xxx
Touka: ¿Pero no crees que tus memorias podrían estar@equivocadas, y no al revés?
Iroha: Qué extraño... Siento que las reconozco.
Iroha: ...
(Player Choice): ¡Mo, mokyu!@(¡Tienes que concentrarte!)
(Player Choice): Mokyu, mokyu.@(Mejor piensa en esto.)
userName: Mo, mokyu!
Iroha: Tienes razón.
userName: Mokyu, mokyu.
Iroha: No. Tengo que sacar esto de mi mente por ahora.
Iroha: Lo más importante ahora es descifrar@como encontrar a las otras.
Yachiyo: Tsuruno y Felicia no vinieron a casa...
Yachiyo: Eso tiene que significar que hay alguna clase de base@donde se están escondiendo.
Yachiyo: No tiene sentido que estén merodeando@alrededor sin parar sin un lugar donde descansar.
Iroha: ...
Momoko: Probablemente hay una sola forma de@investigarlo, incluso si toma algo de tiempo.
Momoko: Todo lo que podemos hacer es rastrear a una de esas@Plumas Negras.
Yachiyo: Si, creo lo mismo.
Iroha: Entonces tengo una sugerencia.
Momoko: ¡¿En serio?!
Iroha: Si, aunque no sé qué tan útil va a ser.
Yachiyo: Solo estoy feliz de escuchar que tienes algunas ideas.@¿Cual es?
Iroha: ―Iroha―@¿Que tal si damos vueltas@y destruimos Rumores otra vez?
Iroha: ―Iroha―@No tienen que ser particularme@peligrosos o dañinos.
Yachiyo: ―Yachiyo―@Para atraer a las Alas de Magius?
Iroha: ―Iroha―@Si.
Iroha: ―Iroha―@Pero esta vez vamos a áreas diferentes@y a atraer un montón de atención.
Iroha: ―Iroha―@Eso podría forzarlas a moverse.
Iroha: ―Iroha―@Y también está la chance de que podamos@descubrir a dónde se están escondiendo.
Momoko: Dejame ver si entendí bien esto.
Momoko: Si luchamos contra un [textBlue:Uwasa] en Sankyo Ward, y las@Alas de Magius se muestran al instante...
Momoko: ¿Eso significa que su base está en alguna parte de ese@vecindario?
Momoko: ¿Es a eso a lo que quieres llegar?
Iroha: Si. Por eso deberíamos cubrir@tantas áreas como podamos.
Yachiyo: Bueno, se merece un intento. Hagámoslo.
Yachiyo: Es mejor hacer algo, cualquier cosa, que@solo estresarnos pensando en ello.
Iroha: ¡Asi es!
Yachiyo: Y sería increíblemente beneficioso si las@Alas de Magius se mostrasen.
Yachiyo: Podríamos atraparlas y hacer que escupan@a donde están manteniendo a todas.
Iroha: Eso definitivamente sería un método más rápido...
Momoko: Ok, ¿entonces a donde quieres que vaya?
Yachiyo: Momoko, no te presiones.
Momoko: ¿Qué? Pero yo...
Yachiyo: Todavía no has arreglado las cosas con Kaede,@¿no?
Momoko: Bueno, uh...
Yachiyo: Quédate con ella.
Yachiyo: Iroha y yo nos encargaremos de los [textBlue:Rumores].
Momoko: ...
Momoko: Gracias, Yachiyo.
Momoko: Pero, hey... Todavía saldré a juntar algunas@Grief Seed para ustedes, ¿esta bien?
Yachiyo: Eso suena perfecto.
Iroha: Vamos, Yachiyo.
Yachiyo: Muy bien.
Yachiyo: Vamos a hacer un tour del área y borrar@los [textBlue:Rumores] mientras tanto.
Momoko: ...