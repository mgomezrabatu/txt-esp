Iroha: Momoko también pensó que sería@difícil hacer que esas dos se reúnan.
Iroha: Si tratamos de forzar a Rena@y Kaede para hablar, lo más@probable es que huyan...
Iroha: Pero trabajando juntas,@nos podremos acercar a ambas,@¡así que debería funcionar!
Iroha: Bueno, al menos eso dijo Momoko...
Iroha: Siento que esto es un poco forzado.@Me pregunto si realmente funcionara...
Momoko: ¡Entonces!
Momoko: Traeré a Kaede cuando salga de la escuela.
Momoko: Iroha, tú esperas a Rena y la capturas.
Momoko: Estará en una reunión del consejo,@así que espérala en la puerta frontal@de la escuela.
Iroha: [chara:100102:effect_emotion_surprise_0][se:7226_shock]E-espera, ¡¿qué?!
Iroha: Pensé que yo me encargaría@de traer a Kaede...
Iroha: Realmente no he hablado con Rena,@excepto en esa pelea del parque.
Momoko: Ya la he perseguido,@por lo que desconfía de mí.
Iroha: Pero con Kaede no sería lo—
Iroha: Ah, pero Rena es...
Momoko: Está tan alerta como un animal salvaje.
Momoko: Así que no tengo más opción@que dejártela a ti, Iroha.
Iroha: ¿Realmente piensas que seré capaz de@llevarla al punto de reunión...?
Momoko: Probablemente será más fácil@traerla aquí con un buen cebo.
Momoko: Dile que Sayuki Fumino dará un@concierto al aire libre.
Momoko: Eso hará que se mueva.
Momoko: Sayuki no es de mis idols favoritas,@¡así que no se lo esperará!
Mitama: Iroha.
Iroha: ¿Mitama...?
Mitama: Momoko es buena cuidando a otros,@pero eso no la hace inteligente.
Mitama: Si yo fuera Rena, me enojaría mucho si alguien@usara algo que amo para engañarme.
Momoko: Ey, ¡¿de qué estás hablando?!
Mitama: Oh, nada…