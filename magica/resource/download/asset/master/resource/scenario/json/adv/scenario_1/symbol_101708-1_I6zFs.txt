﻿Yachiyo: Aquí tienes tu chocolate caliente.
Felicia: Oh, ¡esta ardiendo!
Sana: ¡Gracias!
Felicia: Oh, caliente, caliente. *Soplo* *Soplo*
Sana: *Sip* *Sip*
Felicia: *Glug* *Glug*
Felicia: Yum, tan bueno...
Sana: Es tan relajante...
Iroha: No hay nada como tomar algo caliente cuando@necesitas relajarte.
Sana: Si...
Yachiyo: Muy bien. Vamos directo al punto.
Yachiyo: ¿Cómo lograron escaparse?
Yachiyo: ¿Y donde está Tsuruno? ¿No estaba con ustedes?
Sana: Um, no estoy segura de que le pasó a Tsuruno.
Sana: Se la llevaron a algún lugar.
Yachiyo: ¿Se la llevaron?
Iroha: Oh no...
Iroha: Entonces lo que dijo la Pluma Blanca era verdad.
Pluma Blanca: Retenganlas. Haremos que sigan los pasos de@Tsuruno Yui. <--- Revisar
Pluma Blanca: Llamenlas. Busquen alto y bajo...@Pero nunca recuperaras a tus amigas.
Iroha: Tsuruno...
Yachiyo: ¿Estas diciendo que Mifuyu realmente le hizo algo@a Tsuruno?
Yachiyo: Si eso es verdad, nunca podre@perdonarla.
Sana: ¡N-no!
Sana: ¡Mifuyu no es así!
Yachiyo: Pero ¿se la llevo?
Yachiyo: La Pluma Blanca dijo que no se podía rescatar a@nuestras amigas.
Sana: No, quiero decir que estábamos...
Felicia: ¡Si, si!
Felicia: ¡Esa chica nos salvó las nalgas por completo!
Sana: Si. Esa batalla que tuvimos antes.
Sana: La arreglo para nosotras para que pudieramos@escapar.
Yachiyo: ¿Qué quieres decir?
Iroha: Felicia, Sana, por favor cuéntenos todo.
Sana: Por supuesto.