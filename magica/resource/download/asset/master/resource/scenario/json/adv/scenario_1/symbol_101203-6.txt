Rena: ...¿Por qué me sigues?@Terminamos.
Kaede: Porque... Aun quiero que seamos amigas.
Rena: Para...
Kaede: ¡No, déjame disculparme!
Rena: ¡DIJE QUE PARES!
Kaede: ¡Lo siento, Rena!
Kaede: No pediré que te disculpes@por todo lo que has hecho, así que…
Kaede: ¿Lucharías conmigo otra vez?
Rena: ¡I-idiota!
Rena: ¡¿Por qué te disculpaste?!@¡¿Qué pasa si eso viene y te atrapa?!
Iroha: [chara:100102:effect_emotion_surprise_0][se:7226_shock]¡¿...?!
Iroha: Rena, así que sí crees en la@[textRed:Regla de la Ruptura de Amistades]...
Rena: Ugh...
Iroha: Por eso has evitado a Kaede.@Porque te preocupas por ella, ¿cierto?
Rena: ...¡!
Kaede: Rena... ¿Es verdad?
Rena: Sí...
Rena: ¡Sí, es verdad!
Rena: ¡¿Y qué hay de malo?!@Sí, creo en eso, ¡incluso con mi edad!
Rena: Si Kaede desapareciera porque@dije que [textRed:terminamos]...
Kaede: Pero no me ha pasado nada. ¿Ves?
Kaede: Como dijo Momoko, los rumores son solo rumores.
Rena: ¿En serio?
Kaede: Sí, todo está bien, Rena.
Kaede: ¿Qu-qué? ¡¿Un Laberinto?!
Iroha: …No, algo está mal…
Iroha: No...@Esto no es un Laberinto de Bruja…
Rena: ¿Entonces que es eso de allá…?
Rena: ¿No es un Familiar…?
｜『』『』『』『』!!｜[se:7214_witch_noise_02]
Iroha: ¡C-como debería saberlo!
Kaede: ¡Aah!
Iroha: ¡Kaede!
