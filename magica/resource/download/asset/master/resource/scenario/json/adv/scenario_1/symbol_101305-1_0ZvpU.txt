Iroha: Ugh... ¡Mi corazón se está acelerando!
Iroha: El [textRed:Santuario Séanse]...
Iroha: Puede que me reuna con Ui aquí.
Iroha: ¡Y tal vez con Touka y Nemu!
Mitama: [chara:101700:effect_emotion_surprise_0][se:7226_shock]¡¿Qué?!
Mitama: ¡¿Entonces ahora estás investigando@rumores con Yachiyo?!
Iroha: Sí.
Iroha: Yachiyo está investigando el [textRed:Santuario Séanse]@también, así que me invitó a ir con ella.
Iroha: Se siente tan bien que finalmente@me reconozca.
Iroha: Además, me emociona la idea de finalmete@encontrar a Ui...
Iroha: Así que no dormí mucho anoche.
Mitama: Wow, ¡Que interesante!
Mitama: Nunca imaginé que Yachiyo@invitaría a alguien a trabajar con ella.
Iroha: También me sorprende.
Mitama: De cualquie forma, Iroha...
Iroha: ¿Sí?
Mitama: Intenta no dejarte llavar tanto por un rumor...
Mitama: Siempre en buena idea mantener algo de@duda en algún lugar de ese corazón tuyo.
Iroha: Tienes razón, tedré cuidado.
Iroha: [chara:100102:effect_emotion_surprise_0][se:7226_shock]¡¡Quu-!!
Tsuruno: ¡Hola, Coordinadora!
Mitama: Oh, hola.
Tsuruno: Ah, Iroha, ¡Ahí estás!
Tsuruno: ¡Encontraste el [textRed:Santuario Séanse]!@¡Impresionante! ¡BIEN HECHO!
Iroha: La verdad no hice mucho.@Solo fue suerte.
Tsuruno: ¡Muy bien! ¡Deberiamos calentar un poco@antes de ir con tu amiga!
Iroha: ¡¿Huh?![chara:100102:effect_emotion_surprise_0][se:7226_shock]
Tsuruno: Ánimo, Iroha. ¡Vamos!
Iroha: Whoa, ¡espera un momento, Tsuruno!
Iroha: Yo no quie—
Tsuruno: En sus marcas, lista, ¡VAMOS!
Iroha: ¡¡¡Quuuu!!!
Mitama: Llvará a Iroha y a Tsuruno
Mitama: ¿Qué le pasa a Yachiyo...?
Mitama: Si hubiera sabido que así serían@las cosas...
Mitama: Le hubiera dicho todo sobre el rumor del@[textRed:Santuario Séanse] desde el principio.
