﻿[flashEffect:flashRed2][se:6001_hit_shooting_01_vh][bgEffect:shakeLarge][name:sprite_0:effect_shake]|OARRR...|
Tsuruno: W-wah, ¡estoy cayendo!
Felicia: [flashEffect:flashWhite2][bgEffect:shakeSmall]¡TE ATRAPE!
Felicia: [flashEffect:flashRed2][bgEffect:shakeSmall][chara:100500:effect_shake][se:3001_chara_damage]¡Nnngh!
Felicia: ¡E-eres PESADA!
Tsuruno: ¡Qué mala!
Sana: Se terminó... ¿Cierto?
Iroha: Si, ahora todo lo que queda es que el Laberinto@desaparezca.
Tsuruno: ...
Tsuruno: Hey, chicas, realmente lo siento por todo eso.@Debe haber sido un enorme dolor de trasero.
Yachiyo: ¿De que te estás disculpando? Yo soy la que@necesita pedir perdón.
Tsuruno: Pero me deje lavar el cerebro, y deje el equipo...
Tsuruno: Hasta estuve a punto de asesinar a un monton@de gente...
Yachiyo: Pero, para mejor o peor, terminamos@descubriendo tus verdaderos sentimientos, Tsuruno.
Yachiyo: Creo que eso fue algo bueno. Y además tuve la@chance de re-evaluarme a mi misma.
Iroha: Fue una forma extraña de hacerlo, pero me alegro@de haber podido conocerte mejor.
Iroha: Además, es totalmente la culpa de las Magius que las cosas@terminaran así.
Tsuruno: Si...
Iroha: Las cinco de nosotras vamos a seguir apoyandonos@entre sí mientras avanzamos...
Iroha: También quiero que pensemos en el destino al que@estamos atadas como Chicas Mágicas... transformarnos en Brujas.
Iroha: Y quiero que descubramos alguna forma de@hacer algo al respecto, como un equipo.
Iroha: Pensaremos en una mejor respuesta que la que@encontraron las Alas de Magius...
Yachiyo: Si.
Tsuruno: Ya lo creo.
Felicia: ¡Muy bien, ahora hagamos esa cosa!
Sana: ¿Qué cosa?
Felicia: ¡Esa cosa en la que juntamos nuestras manos!@¡¿Como antes de un partido importante, sabes?!
Tsuruno: ¡Oh, eso lo haría sentir como una reunión!
Felicia: ¡¿Si, no?!
Tsuruno: ¡Muy bien todas, manos adentro!
Tsuruno: Iroha, ¡tú empiezas!
Iroha: [chara:100100:effect_emotion_surprise_0][se:7226_shock]¡¿Yo?!
Tsuruno: Tu eres la líder, ¿cierto?
Iroha: B-bueno sí pero...
Tsuruno: ¡Yep, yep!
Iroha: ¡O-okey entonces!
Iroha: ¡Equipo Mikazuki Villa!
Iroha: ¡V-vamos a, uh, hacerlo!
Yachiyo & Tsuruno: WOOOHOOOOOOOOO!
Felicia & Sana: WOOOHOOOOOOOOO!
¿Huh? ¿Qué estoy haciendo aquí afuera?
Siento como si hubiera estado yendo a algún lado.
¿Un parque de diversiones?
No hay forma de que haya uno aquí afuera@, ¿cierto?
Hombre, muy raro.