Lil' Kyubey: ¡Mokyu!
Iroha: Esto nos llevará al nivel más profundo.
Momoko: Sí, la cámara privada de la bruja@está más adelante.
Momoko: No estaba tan segura del pequeño,@pero realmente nos ha guiado hasta aquí.
Iroha: ¿Lo hicimos antes que Yachiyo?
Momoko: Supongo que sí.@Entremos antes de que llegue.
Iroha: ¡Okay!
¡¡¡GZJGG▲◇◎◎◇▼☆☆ー!!!
Momoko: ¡¡Waa!!
Momoko: ¡Iroha, cuidado!
Iroha: ¡¿Eh?!
Iroha: [se:6256_hit_spike_02_v]¡¡Aagh!!
Iroha: ¿Qué fue eso?@Se sintió como una ola de magia...
Yachiyo: Oh, finalmente lo lograron.
Iroha: (Así que esa era su magia...)
ZJRZJR…☆…◇…
Iroha: Derribó a la bruja con un solo golpe.
Yachiyo: Estaba cansada de esperar,@así que decidí jugar con la bruja.
Momoko: Tienes que estar bromeando...@Eres demasiado rápida...
Yachiyo: ¿Demasiado rápida? Yo diría que ustedes dos son demasiado lentas.
Yachiyo: Momoko, deberías haber sabido que@soy más fuerte que ustedes dos juntas.
Momoko: Ugh...
Yachiyo: Como sea, es bastante obvio quién@es el ganador.
Iroha: ...
Yachiyo: Pero sólo por esta vez,@te daré otra oportunidad.
Iroha: ¿Lo harás?
Yachiyo: Sí.@Derrota a la bruja por ti misma.
Yachiyo: Entonces tendrás mi permiso para quedarte aquí.
Yachiyo: Sin embargo, lamento haberla@debilitado.
Iroha: ...
Momoko: ¡Ya basta!@¡Deja de meterte con ella!
Yachiyo: Qué cosas tan horribles dices. Me lo estoy pensando mejor, no me meto con ella.
Yachiyo: Además, esto no es de tu incumbencia.
Yachiyo: Depende de ella.
Momoko: Ugh...
Yachiyo: ¿Y? ¿Qué vas a hacer?
Iroha: (Como dijo Momoko antes...)
Iroha: (Puede que tenga que volver a Kamihama...)
Iroha: En ese caso, Yo…
Iroha: Voy a derrotar a esta bruja, aquí y ahora.
Yachiyo: Muy bien. Muéstrame que puedes vencer a@una bruja de la ciudad de Kamihama.
Momoko: Puedes hacerlo, Iroha.
Iroha: ¡Si!
Yachiyo: Ponte a ello. La bruja se está moviendo.