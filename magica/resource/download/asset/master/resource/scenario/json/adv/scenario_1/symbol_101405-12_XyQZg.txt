Yachiyo: ¡Iroha, distrae a las de negro!
Yachiyo: ¡Tsuruno y yo abordaremos a las hermanas!
Iroha: ¡Okay!
Tsuruno: ¡Lo tengo!
Yachiyo: Felicia, elimina a los que Iroha@desvía de nosotros.
Felicia: ¡Déjame MACHACARLAS a mi! ¡No soy tan buena@en contenerme!
Kyoko: Espera un segundo.
Felicia: ¡Ugh! ¿Qué pasa?
Felicia: ¡Suéltame, chica... de rojo!
Kyoko: Oye, ¿quieres ir al frente?
Felicia: ¿Eh?
Kyoko: Solamente nos queda media hora.
Kyoko: Solo digo que es un desperdicio tener a todas@nosotras luchando al mismo tiempo.
Felicia: Pero me dijeron que fuera a golpear a esos.
Kyoko: Sí, ¿y?
Kyoko: Mira, tenemos nuestras prioridades.
Kyoko: ¿No te preocupa que ocurra algo horrible?
Felicia: ...Mmm, sí. Preferiría evitar eso.
Kyoko: Así que dejemos esta lucha para ellas.
Kyoko: Tú y yo podemos ir juntas y darle una paliza@a ese Uwasa.
Kyoko: Ese era tu plan antes, ¿no?
Felicia: Ah...
Felicia: ¡Tienes razón!
Kyoko: Golpea cuando encuentres la oportunidad.
Kyoko: Encuentra un hueco y corre directamente a@través de él.
Felicia: Sí, de acuerdo.
Yachiyo: Felicia, ¿qué estás haciendo? ¡Deprisa!
Yachiyo: ¡¡¡Felicia!!!
Yachiyo: ¡Qu—!
Yachiyo: ¡¿Está... con Kyoko?!
Tsukasa: ¡Se está burlando, Tsukuyo!@¡Despreocupandose de nosotras de esa forma!
Tsukuyo: ¡Espera, Tsukasa!
Tsukasa: ...¡¿?!
Tsukuyo: Esas dos... ¡Se dirigen al Uwasa!
Yachiyo: Ya veo...
