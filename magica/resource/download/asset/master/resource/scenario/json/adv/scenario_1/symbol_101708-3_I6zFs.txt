﻿Touka: Hmmmmmm.
Touka: ¡Lo tengo! ¡Será la Señorita Poderosa!
Alina: Yo, Alina, estoy de acuerdo.
Mifuyu: Pero Touka, espera.
Mifuyu: ¿Qué es lo que quieres hacer con Tsuruno?
Touka: ¿No es obvio?
Touka: Eve va a salir del cascarón pronto. ¡No podemos@quedarnos mirando el techo aquí!
Touka: Podría pasar en cualquier momento, el grupo de Iroha@Tamaki no para de destruir[textBlue:Rumores].
Touka: Sin importar cuanto le decimos que no lo hagan.
Touka: Así que ya no tiene sentido que nos tomemos nuestro tiempo@, ¿no?
Touka: Quiero terminar esto ahora.
Mifuyu: Pero...
Touka: ¡Ven entonces, Señorita Poderosa!
Tsuruno: Seguro. Haré lo que sea para ayudar.
Mifuyu: ¡Espera!
Mifuyu: Touka, Alina, por favor…¡diganme! ¡¿Que @van a hacer con ella?!
Alina: Oh, simple.
Alina: Solo haremos que esta poderosa chica@haga algo de asesinato en masa por nosotras.
Mifuyu: [chara:100601:effect_emotion_surprise_0][se:7226_shock]Ases... ¿Qu-Qué están...?!
Touka: Bueno, ellas destruyeron un montón de [textBlue:Rumores],@despues de todo.
Touka: *Risita*
Yachiyo: Ya veo...
Yachiyo: Así que después de que Mifuyu las liberó de su control,@se llevó a Tsuruno.
Iroha: Tsuruno...
Yachiyo: Al menos sabemos que todavía está viva… ¡Es@bueno escucharlo!
Iroha: Lo es, ¡pero no podemos alegrarnos por eso!
Iroha: Quieren hacer que Tsuruno mate a un montón de@personas...
Iroha: ¿Que están Touka y Alina planeando hacer que haga?
Sana: No se...
Sana: Mifuyu dijo que no sabía tampoco.
Felicia: Por eso nos dejó escapar antes de que@nos agarren a nosotras también.