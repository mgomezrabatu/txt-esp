﻿Iroha: Asi que aqui esta el parque ferroviario.
Kanagi: Solo guardan los viejos carriles de tren oxidados@aqui estos días.
Iroha: ...
Iroha: No siento nada aquí.
Yachiyo: No hay señal de gente regular o Chicas Mágicas...
Momoko: ¿Quizás nos equivocamos con el mapa?
Iroha: ¿Así que esta no es su base?
Iroha: No tenemos tiempo hasta que ese parque de@diversiones abra.
Momoko: Si, el tiempo corre.
Yachiyo: ¿Crees que sabían de nuestro plan?
Kanagi: Hmph...
Kanagi: No es muy probable, pero podrían estar en@el nuevo parque ferroviario.
Yachiyo: ¿Así que podrían haberse trasladado entonces?
Kanagi: No está fuera del reino de las posibilidades.@Ire a verlo por mi cuenta.
Kanagi: Conozco el área, así que tiene sentido que yo@lo revise.
Yachiyo: Si no te molesta.
Kanagi: Muy bien. Volveré pronto.
Iroha: Quizás estén siendo cuidadosas porque@dimos vueltas destruyendo todos esos Rumores.
Yachiyo: Es cierto que fuimos de barrio en barrio@rapidamente, y además derrotamos a muchos de ellos.
Yachiyo: Tiene sentido que sean cautelosas.
Iroha: ¿Qué hacemos ahora?
(Player Choice): Mokyu.@(¡Investigar el área!)
(Player Choice): ¡Mokyukyu!@(¡Revisar otro lugar!)
userName: Mokyu.
Iroha: Si, deberíamos investigar este lugar@ en profundidad antes de hacer otra cosa.
userName: ¡Mokyukyu!
Iroha: Si hacemos eso, podría ser difícil encontrarnos de nuevo@con Kanagi.
Iroha: Creo que deberíamos investigar este lugar@ en profundidad antes de hacer otra cosa.
Momoko: Si, todavía hay una chance de que hayan@encontrado alguna forma de esconderse aquí.
Yachiyo: Busquemos lo mejor que podamos.