Tsuruno: Juguemos un pequeño juego de asociación de palabras.@¡Y entonces entenderás mi método!
Tsuruno: ¡Yo digo [textRed:Chica Mágica],@tú dices [textBlue:Rumor]!♪
Tsuruno: ¡Yo digo [textBlue:Rumor],@tú dices Alas de Magius!♪
Tsuruno: ¡Yo digo Alas de Magius,@tu dices Tsukuyo Amane!♪
Felicia: ¿Ehhh? ¿Tsukuyo Amane? Creo haber@escuchado ese nombre antes...
Tsuruno: Ella es una de esas Alas de Magius que@visten de blanco. ¡Una de las gemelas!
Tsukuyo: Me llamo Tsukuyo Amane, una Pluma Blanca de@las Alas de Magius.
Felicia: ¡La que habla como señora grande!
Tsuruno: ¡Sí!
Tsuruno: ¡De cualquier forma!
Tsuruno: ¡Yo digo Tsukuyo Amane,@tú dices uniforme de la Academia para chicas de Mizuna!♪
Tsuruno: Lo que significa que...iremos a la@Acedemia para chicas de Mizuna.
Tsuruno: ¡Que la "Operación: Capturar a Tsukuyo Amane"@EMPIECE!
Felicia: Capturar...@¡Oh, es suena divertido!
Tsuruno: ¡Ella sabrá mucho sobre los rumores, y la@[textRed:Chica de la Onda de Radio]! ¡Podemos preguntarle!
Felicia: Muy bien, estamos aquí. ¿Estás segura de que todavía@estará por aquí?
Tsuruno: ¡No hay necesidad de preocuparse por eso!
Tsuruno: ¡Me hice pasar por un familiar suyo y@le dije a la escuela que vendría!
Tsuruno: Les pedí que le dijeran que viniera a reunise conmigo.
Felicia: Whoa.
Tsuruno: Ahora llamaré a la escuela de nuevo.
Tsuruno: Les diré, "Ah, no podré ir después de@todo, así que por favor manden a Tsukuyo a casa."
Tsuruno: Y entonces, justo cuando se vaya de la escuela...
Felicia: BAM!
Felicia: ¡Whoa, Tsuruno, eres muy inteligente!
Tsuruno: *Risilla*
Felicia: ¡Eres como una mente maestra malvada!
Tsuruno: [chara:100301:effect_emotion_surprise_0][se:7226_shock]¡¿Qué?! ¡¿Malvada?!
Tsuruno: ¡Yo peleo por la justicia!
Tsukuyo: Me pregunto por qué llamaría a la escuela...
Tsukuyo: Es cierto que no tengo mis lecciones hoy, pero@de pensar que vendría por mí a la escuela...
Tsukuyo: Supongo que cosas raras si pasan@de vez en cuando...
Felicia: [bgEffect:shakeSmall][flashEffect:flashWhite2]¡Ahora!
Tsuruno: [bgEffect:shakeSmall][flashEffect:flashWhite2]¡La tenemos!
Tsukuyo: [chara:101801:effect_emotion_surprise_0][se:7226_shock][bgEffect:shakeSmall]¡¿Queeeeeé?!
Tsukuyo: ¡U-ustedes dos! ¡Tsuruno Yui y Felicia Mitsuki!
Tsukuyo: ¿Cómo supieron que venía a esta escuela?
Tsuruno: Vi tu uniforme el otro día.
Tsukuyo: [chara:101801:effect_emotion_sad_0][se:7224_fall_into]*Jadeo*@Me descuidé...
Tsukuyo: Espera, entonces la llamada de hace rato, ¿esas también@fueron ustedes?
Tsuruno: *Risilla* Te sorprendí, ¡¿No?!
Tsukuyo: Que espantoso... El trabajo de un verdadero@secuestrador...
Tsuruno: [chara:100301:effect_emotion_sad_0][se:7224_fall_into]Ugh... Tú también lo ves de esa manera...
Tsuruno: Pues... Er, ¿lo siento?
Tsukuyo: Ah, no, no es nada... Por favor no@te preocupes.
Tsukuyo: Espera, ¡¿Por qué debería perdonarte?!
Tsukuyo: ¿Qué podrías querer de mí?
Tsuruno: ¿Te importaría venir con nosotras? Solo@queremos que respondas algunas preguntas.
Tsukuyo: E-es esto...
Tsukuyo: ¡¿Es esto algún tipo de interrogatorio?! ¡¿Están@planeando torturarme?!
Tsuruno: No, por su puesto que no.
Felicia: Sí, solo queremos hablar contigo.
Tsukuyo: ...
Tsukuyo: Tengo planes más tarde, así que si no tomará@mucho tiempo... Me parece bien, las atenderé...
Tsukuyo: No parece que pueda escapar de cualquier modo.
