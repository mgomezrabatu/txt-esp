Iroha: ¡Ah…!
Rena: ¡Hmph! Sabes, una vez que me acerco a ti,@no tienes esa ventaja de largo alcance.
Iroha: (Voy a perder...)
Rena: ¡Toma esto!
Iroha: ¡Agh!
Iroha: ¡¿Huh?! ¡¿Q-qué?!
Rena: ¡Te tengo!
Iroha: ¡Agh![se:6259_hit_spike_03_v]
Rena: *Huff* Déjanos en paz.@No tienes ni idea de lo que está pasando.
Rena: ¡Idiota!
Iroha: Auch…
Momoko: Maldición... Se escapó...
???: ...
Momoko: Toma esto. Yo invito.
Iroha: Ah, no tienes qué...
Momoko: Tómalo como una disculpa por@haberte metido en nuestra discusión.
???: Además, no te he agradecido@adecuadamente por salvarme antes...
Iroha: Bien...@Si insisten...
Momoko: Ya veo.@¿Así que ustedes ya se conocian antes?
Iroha: Sí. Sin embargo, no tuvimos@tiempo de presentarnos.
Kaede: Oh... Es cierto.@Soy [textRed:Kaede Akino].
Iroha: Soy Iroha Tamaki.@Encantada de conocerte, Kaede..
Kaede: Encantado de conocerte a ti también.
Iroha: Entonces, ¿en qué consistió esa pelea?
Kaede: Ah… Um...
Momoko: Se está callando de nuevo...
Momoko: Ha estado así desde que Rena se fue…
Momoko: Bueno, no hay nada especial@en que ustedes dos se peleen así.
Momoko: No voy a forzarte a hablar de ello.
Iroha: ¿Realmente se pelean tan a menudo...?
Momoko: Ah, sí. Tan a menudo como@se llenan de dulces.
Kaede: Detente, Momoko.@Es muy embarazoso....
Momoko: Mi error. Normalmente es Rena@quien lo inicia, de todos modos.
Momoko: Supongo que hoy también fue así.@Apuesto a que mañana dirá que lo siente.
Momoko: Ella siempre es así... Una vez que@se calme, se arrepentirá de lo que dijo.
Iroha: ...
Yachiyo: Escucha...
Yachiyo: No deberías decir eso en Kamihama.
Yachiyo: Especialmente cuando estás@discutiendo con un amigo.
Yachiyo: Quedarás atrapada por [textRed:el Rumor de@La regla de la ruptura de amistades]...
Iroha: Kaede... ¿Seguro que está bien decir@que has [textRed:terminado] con Rena?
Kaede: ¿Eh? ¿Por qué lo preguntas?
Iroha: He oído que hay un rumor sobre [textRed:La regla de la@ruptura de amistades] y lo peligroso que es—
Momoko: ...¡¿?![chara:101001:effect_emotion_surprise_0][se:7226_shock]
Momoko: Iroha, ¿quién te ha dicho eso?
Iroha: Um... Lo escuché de Yachiyo...
Momoko: Ja ja ja. Eso es lo que pensé.
Momoko: No te tomes lo que dice demasiado en serio.
Momoko: Es que... Está loca por los rumores.
Momoko: Se cree casi cualquier@rumor que se encuentre.
Iroha: ¿Es así…?
Momoko: Sí. Intenta no preocuparte.
Momoko: Los rumores son sólo rumores. No son ciertos.
