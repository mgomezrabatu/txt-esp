Momoko: Hmmm...
Momoko: ¡¡Ugggh!!
Momoko: [chara:101001:effect_emotion_angry_0][se:7219_strain]¡¡¡Aaaaaahhh!!!
Momoko: ¡¿Por qué tengo que aguantar todo esto?!
Momoko: ¡No puedo soportarlo más!@¡Voy a intervenir!
Momoko: ¡Llámenme sobreprotectora si quieren!
Momoko: ¡Voy a ponerlas cara a cara!
Mitama: Ah, sabía que llegaría a esto...
Iroha: Si Momoko se descontrola...
Mitama: Bueno, la situación no mejorará...
Iroha: (Ojalá hubiera algo que pudiera hacer...)
Iroha: (¿Sería entrometerse si tratara de ayudar?)
Iroha: ...
Iroha: Um, Mitama.
Mitama: ¿Sí?
Iroha: Si ocurre algo, por favor, avísame.
Iroha: Voy a seguir buscando a Ui.
Mitama: Claro, te avisaré.
Iroha: (Es más fácil decirlo que hacerlo.@Mi búsqueda se ha topado con un muro...)
Iroha: Hmm...
Iroha: (Me pregunto dónde viven las familias de Touka@y Nemu...)
Iroha: (Quizás sus compañeros de clase lo sepan...)
Iroha: (No...)
Iroha: (Estaban tomando clases@en el hospital al igual que Ui...)
Iroha: Hmm...
Iroha: [chara:100102:effect_ef_adv_01][se:7201_detect_magic]*Susto*
Iroha: ¡Siento la magia de una Bruja...!
(Player Choice): ¡Puy! (¡A su derecha!)
(Player Choice): ¡Puy! (¡A tu izquierda!)
userName: ¡Puy!
Iroha: ¡Por aquí!
userName: ¡Puy!
Iroha: ¡Por aquí!
Iroha: ¡Oh no! ¡Están perdiendo...!
Iroha: (¿Eh...? Esto me resulta@extrañamente familiar...)
Iroha: (De todos modos,@¡Tengo que apresurarme a rescatarla!)
Iroha: Debe haber una chica mágica en algún lugar...
Iroha: Si pudiera sentir su magia...
Iroha: *Jadeo*@Lo siento... ¿Kaede?!
Iroha: (¡No es solo familiar...@Es lo mismo que antes...!)
Kaede: ¡¡Awww!!
Iroha: ¡Kaede!@¡¡¡Estoy en camino, aguanta!!!
