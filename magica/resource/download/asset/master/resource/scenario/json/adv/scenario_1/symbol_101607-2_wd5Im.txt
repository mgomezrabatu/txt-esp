Iroha: Ehh...¿dejamos las luces prendidas?
Yachiyo: Definitivamente las apagué cuando me fui.
Iroha: ...¿Serán Felicia y las demás?
Iroha: ¡Felicia! ¡Sana!
Momoko: Um, lo siento. Solo soy yo...
Iroha: [chara:100101:effect_emotion_sad_0][se:7224_fall_into]Oh no...
Momoko: [chara:101001:effect_emotion_surprise_0][se:7226_shock]¡¿Ehh?! ¡¿Iroha?! ¡¿Soy tan decepcionante?!
Momoko: Ya se que me dicen Momoko Inoportuna,@pero no es para tanto...
Yachiyo: Momoko...
Momoko: Yachiyo...
Yachiyo: ¿Que sucede? Irrumpiendo en mi casa@de esta manera...
Momoko: Tenía asuntos que tratar aquí, así que me permití entrar.
Momoko: Por cierto, deberías de cambiar el escondite@de tu llave de emergecia de vez en cuando.
Yachiyo: Eso no te concierne.
Yachiyo: De quier manera, estás aquí por nosotras, ¿no es así?
Momoko: Oh, ¿te diste cuenta?
Yachiyo: Sí.
Yachiyo: Estoy segura de que cuando les contaste a Rena y Kaede la@verdad...
Yachiyo: Que también les contaras sobre@mi decisión de dehacer el equipo.
Momoko: Sip, eso mismo.
Momoko: Y quería encargarme de los cabos sueltos,@como debí hacer hecho hace mucho tiempo.
Momoko: Así que me decidí a venir a confrontarte sobre@ello, y esta vez no me voy a rendir.
Yachiyo: No necesitas confrontarme sobre nada.@Te lo diré todo...
Yachiyo: Depués de todo lo que ha pasado, te mereces siquiera@eso.
Momoko: ¿Qué te hizo cambiar de opinión tan repentinamente?
Yachiyo: Iroha se esforzó al límite para convenserme que@me equivocaba...
Momoko: ¡¿De verdad?!
Iroha: Um, sí...
Iroha: Yachiyo empezó a decir que quería dehacer@el equipo de repente, así que...
Momoko: Esperaba que algo así sucediera, pero wow.
Momoko: No sé si es buen o mal tiempo...
Momoko: Como sea, poniendo eso de lado por un momento. ¿Dijiste@que ibas a explicar?
Yachiyo: Sí...
Yachiyo: No estoy segura si estarás satisfecha con@la respuesta.
Yachiyo: Pero sí, esoty lista para contarte...
Yachiyo: Te diré por qué deshice nuestro equipo... Y que@pensaba en aquel entonces...
