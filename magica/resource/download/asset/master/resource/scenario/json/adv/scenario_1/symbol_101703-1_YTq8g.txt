﻿Iroha.: Me pregunto cómo estará el papa@de Tsuruno...
Yachiyo: Probablemente no muy bien.
Yachiyo: Tanto su esposa como su madre no han vuelto a@casa hace tiempo.
Yachiyo: Tsuruno era la única en la que el@podia confiar.
Iroha.: Si...
Yachiyo: Él maneja un restaurante, así que es muy probable que haga lo mejor que pueda para mantenerlo abierto.
Yachiyo: Pero no creo que sea una persona tan@fuerte.
Yachiyo: Existe la chance de que cierre el@restaurante por un tiempo.
Iroha.: Tsuruno y Felicia no están para ayudar asi que@podria no tener otra opción.
Yachiyo: No me sorprendería si estuviera poniendo@volantes para tratar de encontrar a Tsuruno.
Yachiyo: Es su amada hija, después de todo...