Iroha: D-de verdad nos colamos...
Tsuruno: ¡Ja! Es fácil para nostras Chicas Mágicas.
Tsuruno: ¡¿Y ahora qué?!@¡¿Qué hacemos ahora?!
Yachiyo: Shhh.@Tsuruno, ¡Guarda silencio!
Tsuruno: ¡Está bien!
Yachiyo: Bien, permítanme contarles el Rumor.
Yachiyo: Les diré que tienen que hacer@para ver a la gente que quieren ver.
Iroha: Okay.
¿Ya lo escuchaste? ¿Quién te lo dijo?@El Rumor del Santuario Séanse.
¿Un familiar? ¿Un amante? ¿Un extraño?@Si ansías ver a alguien,@déjaselo al dios del Templo Mizuna!
Escribe su nombre en una placa de madera bendecida,@visita el templo, y ofrece una oración edecuada.@El dios los trará contigo.
¡Pero cuidado! ¡Cuidado!@Dicen que te perderás en la dicha@y nunca podrás regresar a casa.
¡Ahhh! ¡Que aterrador!
Yachiyo: Y así va.
Yachiyo: Así que tendremos que usar una placa de madera@de este Templo.
Iroha: Entonces, una vez que me reúna con Ui,@todo lo que tengo que hacer es llevarla a casa.
Iroha: Incluso si la tengo que llevar arrastrando.
Yachiyo: Sí. Deberíamos prepararnos para hacer justo eso.
Yachiyo: Las dos tú...y yo...
Iroha: ¡Oh!@Pero no tenemos ninguna placa de madera.
Yachiyo: Compré algunas.@¿Pensaste que vedría con las manos vacías?
Iroha: ¡G-gracias!
Yachiyo: ¿Les parece si avanzamos?
Tsuruno: Uh... Um... ¿Yachiyo?
Yachiyo: ¿Hm? ¿Qué? @¿Por qué estás extendiendo la mano?
Tsuruno: Um... ¿Dónde está mi placa?
Yachiyo: No traje una para tí.
Yachiyo: Se suponía que solo vendríamos@Iroha y yo hoy.
Yachiyo: No se me hubiera ocurrido@traer una para tí.
Tsuruno: ¡Um... Ummm... Ummm!
Yachiyo: Poner caras no te conseguirá una placa.@¿Para que quieres una?
Tsuruno: ¿Qué?
Tsuruno: ...
Tsuruno: ¡Ahhh! [chara:100301:effect_emotion_surprise_0][se:7226_shock]
Yachiyo: ¿Ves? No necesitas una, ¿o sí?
Tsuruno: Pues, estamos buscando a la misma@persona, así que...
Tsuruno: Supongo que eso quiere decir que no tendré que orar.@¡Lo que significa que no necesito una!
Tsuruno: Entonces yo, Tsuruno Yui, serviré como su@escudo... ¡protegiéndolas mientras oran!
Tsuruno: Estarías en problemas si una Uwasa@se apareciera de repente.
Yachiyo: Siempre eres tan apresurada...
Yachiyo: Es bueno que pienses rápido,@pero trata de no distraerte demasiado.
Iroha: ¿Pensar rápido?
Tsuruno: ¡También soy la más poderosa en la escuela!
Iroha: ¿La más poderosa?
Yachiyo: Está diciendo que saca las mejores notas.
Iroha: ¡¿Queeeeé?!
