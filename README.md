# txt-esp
Este repo es solo para archivos de texto ya traducidos, ya crearemos el repo para json cuando tengamos suficientes

# Nombres y formato
Para evitar confusiones, los archivos que se suban deben llevar el mismo nombre que el archivo original correspondiente.<br/>
Ejemplo:<br/>
"symbol_101203-2.txt"       ✔️<br/>
"symbol_101203-2-esp.txt"   ❌<br/>
"esp_symbol_101203-2.txt"   ❌
